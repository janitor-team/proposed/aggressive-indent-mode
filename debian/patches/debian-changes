The Debian packaging of aggressive-indent-mode is maintained in git,
using the merging workflow described in dgit-maint-merge(7).  There
isn't a patch queue that can be represented as a quilt series.

A detailed breakdown of the changes is available from their canonical
representation - git commits in the packaging repository.  For
example, to see the changes made by the Debian maintainer in the first
upload of upstream version 1.2.3, you could use:

    % git clone https://git.dgit.debian.org/aggressive-indent-mode
    % cd aggressive-indent-mode
    % git log --oneline 1.2.3..debian/1.2.3-1 -- . ':!debian'

(If you have dgit, use `dgit clone aggressive-indent-mode`, rather
than plain `git clone`.)

A single combined diff, containing all the changes, follows.
--- /dev/null
+++ aggressive-indent-mode-1.9.0/.gitignore
@@ -0,0 +1,3 @@
+/debian/files
+/debian/elpa-aggressive-indent*
+/debian/.debhelper
--- aggressive-indent-mode-1.9.0.orig/README.md
+++ aggressive-indent-mode-1.9.0/README.md
@@ -1,4 +1,4 @@
-aggressive-indent-mode [![Melpa](http://melpa.org/packages/aggressive-indent-badge.svg)](http://melpa.org/#/aggressive-indent) [![Melpa-Stable](http://stable.melpa.org/packages/aggressive-indent-badge.svg)](http://stable.melpa.org/#/aggressive-indent)
+aggressive-indent-mode
 ======================
 
 `electric-indent-mode` is enough to keep your code nicely aligned when
@@ -20,11 +20,7 @@ than `electric-indent-mode`.
 
 ### Instructions ###
 
-This package is available from Melpa, you may install it by calling
-
-    M-x package-install RET aggressive-indent
-
-Then activate it with
+Activate this package with
 
     (add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)
     (add-hook 'css-mode-hook #'aggressive-indent-mode)
@@ -36,16 +32,6 @@ every programming mode, you can do somet
     (global-aggressive-indent-mode 1)
     (add-to-list 'aggressive-indent-excluded-modes 'html-mode)
 
-#### Manual Installation ####
-
-If you don't want to install from Melpa, you can download it manually,
-place it in your `load-path` along with its dependency `cl-lib` (which
-you should already have if your `emacs-version` is at least 24.3).
-
-Then require it with:
-
-    (require 'aggressive-indent)
-
 ### Customization ###
 
 The variable `aggressive-indent-dont-indent-if` lets you customize
@@ -62,4 +48,4 @@ following clause:
 
 ## Contribute ##
 
-[![Gratipay](https://cdn.rawgit.com/gratipay/gratipay-badge/2.1.3/dist/gratipay.png)](https://gratipay.com/Malabarba) 
+Make a donation to the package author: <https://gratipay.com/Malabarba>
--- aggressive-indent-mode-1.9.0.orig/aggressive-indent.el
+++ aggressive-indent-mode-1.9.0/aggressive-indent.el
@@ -4,7 +4,7 @@
 
 ;; Author: Artur Malabarba <emacs@endlessparentheses.com>
 ;; URL: https://github.com/Malabarba/aggressive-indent-mode
-;; Version: 1.8.4
+;; Version: 1.9.0
 ;; Package-Requires: ((emacs "24.1") (cl-lib "0.5"))
 ;; Keywords: indent lisp maint tools
 ;; Prefix: aggressive-indent
